a52dec 0.7.4
aalib 1.4rc5
accountsservice 22.08.8
acl 2.3.1
acpi 1.7
acpid 2.0.33
adapta-maia-theme 3.94.0.149
adobe-source-code-pro-fonts 2.038ro+1.058it+1.018var
adwaita-icon-theme 42.0+r1+gc144c3d75
alacritty 0.10.1
alsa-card-profiles 1:0.3.52
alsa-firmware 1.2.4
alsa-lib 1.2.7
alsa-oss 1.1.8
alsa-plugins 1:1.2.6
alsa-topology-conf 1.2.5.1
alsa-ucm-conf 1.2.7
alsa-utils 1.2.7
aom 3.3.0
apparmor 3.0.4
appstream-glib 0.7.18
arandr 0.1.10
archlinux-appstream-data 20220512
archlinux-keyring 20220424
argon2 20190702
artwork-i3 20190912
at-spi2-atk 2.38.0
at-spi2-core 2.44.1
atk 2.38.0
atkmm 2.28.2
attr 2.5.1
audit 3.0.8
autoconf 2.71
automake 1.16.5
autossh 1.4g
avahi 0.8+22+gfd482a7
b43-fwcutter 019
babl 0.1.90
bash 5.1.016
bashrc-manjaro 5.1.016
bat 0.21.0
bauh 0.10.3
bc 1.07.1
beautyline 20220527
binutils 2.38
bison 3.8.2
blas 3.10.1
blueman 2.2.4
bluez 5.64
bluez-libs 5.64
bluez-utils 5.64
bmenu 0.18
boost-libs 1.78.0
botan 2.19.2
box2d 2.4.1
brave-bin 1:1.39.122
bridge-utils 1.7.1
brotli 1.0.9
btrfs-progs 5.18.1
bubblewrap 0.6.2
bzip2 1.0.8
c-ares 1.18.1
ca-certificates 20210603
ca-certificates-mozilla 3.79
ca-certificates-utils 20210603
cairo 1.17.6
cairomm 1.14.3
cantarell-fonts 1:0.303.1
cdparanoia 10.2
celt 0.11.3
ceph-libs 15.2.14
chromaprint 1.5.1
cifs-utils 6.15
ckbcomp 1.208
clucene 2.3.3.4
cmus 2.9.1
code-server 4.4.0
colord 1.4.6
colord-sane 1.4.6
confuse 3.3
containerd 1.6.5
copyq 6.1.0
coreutils 9.1
cpupower 5.18
cronie 1.6.1
cryptsetup 2.4.3
cups 1:2.4.2
cups-filters 1.28.15
cups-pdf 3.0.1
cups-pk-helper 0.2.6
curl 7.83.1
dav1d 1.0.0
db 5.3.28
dbus 1.14.0
dbus-glib 0.112
dbus-python 1.2.18
dconf 0.40.0
desktop-file-utils 0.26
device-mapper 2.03.16
dfc 3.1.1
dhclient 4.4.3
dhcpcd 9.4.1
diffutils 3.8
ding-libs 0.6.2
discord 0.0.18
dkms 3.0.3
dmenu 5.1
dmidecode 3.3
dmraid 1.0.0.rc16.3
dnsmasq 2.86
dnssec-anchors 20190629
docker 1:20.10.16
dos2unix 7.4.3
dosfstools 4.2
double-conversion 3.2.0
downgrade 11.2.0
dropbox 150.4.5000
dunst 1.8.1
dunstify 1.7.3
e2fsprogs 1.46.5
ecryptfs-utils 111
efibootmgr 17
efivar 38
egl-wayland 2:1.1.10
eglexternalplatform 1.1
electron 18.2.4
electron13 13.6.9
electron17 17.4.3
element-desktop 1.10.13
element-web 1.10.13
elfutils 0.187
enchant 2.3.3
entr 5.0
epdfview 0.1.8
exfat-utils 1.3.0
exiv2 0.27.5
exo 4.16.3
expac 10
expat 2.4.8
f2fs-tools 1.15.0
faac 1.30
faad2 2.10.0
fakeroot 1.29
fcitx 4.2.9.8
fcitx-configtool 0.4.10
fcitx-mozc 2.26.4360.102.gca82d39
fcitx-qt5 1.2.7
feh 3.8
ffmpeg 2:5.0
ffmpeg4.4 4.4.1
ffmpegthumbnailer 2.2.2
fig2dev 3.2.8.b
file 5.41
filesystem 2022.06.08
filezilla 3.60.1
findutils 4.9.0
fish 3.4.1
flac 1.3.4
flameshot 11.0.0
flex 2.6.4
fltk 1.3.8
fluidsynth 2.2.7
fmt 8.1.1
fontconfig 2:2.14.0
foomatic-db 3:20220328
foomatic-db-engine 4:20220521
foomatic-db-gutenprint-ppds 5.3.4
freeglut 3.2.2
freetype2 2.12.1
fribidi 1.0.12
fuse-common 3.11.0
fuse2 2.9.9
fuse3 3.11.0
fzf 0.30.0
galculator 2.1.4
garcon 4.16.1
gawk 5.1.1
gc 8.2.0
gcab 1.4
gcc 12.1.0
gcc-libs 12.1.0
gcolor2 0.4
gcr 3.41.0
gd 2.3.3
gdbm 1.23
gdk-pixbuf2 2.42.8
gegl 0.4.36
gendesk 1.0.9
gettext 0.21
ghc-libs 9.0.2
ghostscript 9.56.1
giflib 5.2.1
gimp 2.10.30
git 2.36.1
glew 2.2.0
glib-networking 1:2.72.0
glib2 2.72.2
glibc 2.35
glibc-locales 2.33
glibmm 2.66.4
glslang 11.9.0
glu 9.0.2
gmp 6.2.1
gnome-keyring 1:42.1
gnome-themes-extra 3.28+r6+g45b1d457
gnu-netcat 0.7.1
gnupg 2.2.35
gnuplot 5.4.3
gnutls 3.7.6
gobject-introspection-runtime 1.72.0
goldendict 1.5.0RC2
google-chrome 102.0.5005.115
gparted 1.4.0
gpgme 1.17.1
gpm 1.20.7.r38.ge82d1a6
gptfdisk 1.0.9
graphene 1.10.8
graphicsmagick 1.3.38
graphite 1:1.3.14
graphviz 4.0.0
grep 3.7
groff 1.22.4
grub 2.06.r261.g2f4430cc0
grub-theme-manjaro-dev 18.0
gsettings-desktop-schemas 42.0
gsfonts 20200910
gsl 2.7.1
gsm 1.0.19
gspell 1.10.0
gssdp 1.4.0.1
gssproxy 0.9.1
gst-libav 1.20.2
gst-plugins-bad 1.20.2
gst-plugins-bad-libs 1.20.2
gst-plugins-base 1.20.2
gst-plugins-base-libs 1.20.2
gst-plugins-good 1.20.2
gst-plugins-ugly 1.20.2
gstreamer 1.20.2
gtest 1.11.0
gtk-engine-murrine 0.98.2
gtk-update-icon-cache 1:4.6.5
gtk2 2.24.33
gtk3 1:3.24.34
gtkmm 1:2.24.5
gtkmm3 3.24.6
gtksourceview3 3.24.11+28+g73e57b57
gtksourceview4 4.8.3
gts 0.7.6.121130
gufw 22.04
guile 2.2.7
gupnp 1:1.4.3
gupnp-igd 1.2.0
gutenprint 5.3.4
gvfs 1.50.2
gvfs-afc 1.50.2
gvfs-gphoto2 1.50.2
gvfs-smb 1.50.2
gzip 1.12
harfbuzz 4.3.0
harfbuzz-icu 4.3.0
haskell-aeson 1.5.6.0
haskell-assoc 1.0.2
haskell-attoparsec 0.14.4
haskell-base-compat 0.12.1
haskell-base-compat-batteries 0.12.1
haskell-base-orphans 0.8.6
haskell-bifunctors 5.5.12
haskell-comonad 5.0.8
haskell-data-fix 0.3.2
haskell-diff 0.4.1
haskell-distributive 0.6.2.1
haskell-dlist 1.0
haskell-erf 2.0.0.0
haskell-ghc-bignum-orphans 0.1.1
haskell-hashable 1.4.0.2
haskell-indexed-traversable 0.1.2
haskell-integer-logarithms 1.0.3.1
haskell-primitive 0.7.4.0
haskell-quickcheck 2.14.2
haskell-random 1.2.1.1
haskell-regex-base 0.94.0.2
haskell-regex-tdfa 1.3.1.2
haskell-scientific 0.3.7.0
haskell-splitmix 0.1.0.4
haskell-strict 0.4.0.1
haskell-tagged 0.8.6.1
haskell-th-abstraction 0.4.3.0
haskell-these 1.1.1.1
haskell-time-compat 1.9.6.1
haskell-transformers-compat 0.7.1
haskell-unordered-containers 0.2.19.1
haskell-uuid-types 1.0.5
haskell-vector 0.12.3.1
haveged 1.9.18
hdparm 9.63
hexchat 2.16.1
hicolor-icon-theme 0.17
hidapi 0.12.0
hplip 1:3.22.4
htop 3.2.1
http-parser 2.9.4
hunspell 1.7.0
hwdata 0.360
hwinfo 21.80
hyphen 2.8.8
i3-default-artwork 20190912
i3-gaps 4.20.1
i3-help 20180110
i3-scripts 20200804
i3-scrot 2.1
i3blocks 1.5
i3exit 20201126
i3lock 2.14
i3status 2.14
iana-etc 20220603
ibus 1.5.26
icu 71.1
ijs 0.35
imagemagick 7.1.0.37
imath 3.1.5
imlib2 1.9.0
inetutils 2.2
iniparser 4.1
inkscape 1.2
intel-ucode 20220510
inxi 3.3.18.1
iotop 0.6
iproute2 5.18.0
iptables 1:1.8.8
iputils 20211215
iso-codes 4.10.0
iw 5.19
jack2 1.9.21
jansson 2.14
jasper 3.0.4
java-runtime-common 3
jbig2dec 0.19
jfsutils 1.1.15
jq 1.6
jre-openjdk 18.0.1.1.u2
jre-openjdk-headless 18.0.1.1.u2
js78 78.15.0
json-c 0.16
json-glib 1.6.6
kauth 5.94.0
kbd 2.5.0
kconfig 5.94.0
kcoreaddons 5.94.0
keepassxc 2.7.1
keyutils 1.6.3
kguiaddons 5.94.0
kitemmodels 5.94.0
kmod 29
knotifications 5.94.0
krb5 1.19.3
kvantum 1.0.2
kvantum-manjaro 0.13.5+1+g333aa00
kwindowsystem 5.94.0
l-smash 2.14.5
lame 3.100
lapack 3.10.1
lcms2 2.13.1
ldb 2:2.5.1
ldns 1.8.1
lensfun 1:0.3.3
less 1:590
lib2geom 1.1
lib32-brotli 1.0.9
lib32-bzip2 1.0.8
lib32-curl 7.83.1
lib32-e2fsprogs 1.46.5
lib32-expat 2.4.8
lib32-flex 2.6.4
lib32-freeglut 3.2.2
lib32-gcc-libs 12.1.0
lib32-glew 2.2.0
lib32-glibc 2.35
lib32-glu 9.0.2
lib32-icu 71.1
lib32-keyutils 1.6.3
lib32-krb5 1.19.3
lib32-libdrm 2.4.111
lib32-libelf 0.187
lib32-libffi 3.4.2
lib32-libglvnd 1.4.0
lib32-libice 1.0.10
lib32-libidn2 2.3.2
lib32-libldap 2.6.2
lib32-libpciaccess 0.16
lib32-libpsl 0.21.1
lib32-libsm 1.2.3
lib32-libssh2 1.10.0
lib32-libunistring 1.0
lib32-libunwind 1.6.2
lib32-libx11 1.8
lib32-libxau 1.0.9
lib32-libxcb 1.15
lib32-libxcrypt 4.4.28
lib32-libxdamage 1.1.5
lib32-libxdmcp 1.1.3
lib32-libxext 1.3.4
lib32-libxfixes 6.0.0
lib32-libxi 1.8
lib32-libxml2 2.9.14
lib32-libxmu 1.1.3
lib32-libxrandr 1.5.2
lib32-libxrender 0.9.10
lib32-libxshmfence 1.3
lib32-libxt 1.2.1
lib32-libxxf86vm 1.1.4
lib32-llvm-libs 13.0.1
lib32-lm_sensors 1:3.6.0.r41.g31d1f125
lib32-mesa 22.1.1
lib32-mesa-demos 8.4.0
lib32-ncurses 6.3
lib32-nvidia-utils 515.48.07
lib32-openssl 1:1.1.1.n
lib32-readline 8.1.002
lib32-util-linux 2.38
lib32-vulkan-icd-loader 1.3.213
lib32-wayland 1.20.0
lib32-xz 5.2.5
lib32-zlib 1.2.12
lib32-zstd 1.5.2
libabw 0.1.3
libaio 0.3.113
libao 1.2.2
libappindicator-gtk3 12.10.0.r296
libarchive 3.6.1
libass 0.15.2
libassuan 2.5.5
libasyncns 1:0.8+r3+g68cd5af
libatasmart 0.19
libatomic_ops 7.6.12
libavc1394 0.5.4
libavif 0.10.1
libavtp 0.2.0
libb2 0.98.1
libblockdev 2.26
libbluray 1.3.0
libbpf 0.8.0
libbs2b 3.1.0
libbsd 0.11.6
libburn 1.5.4
libbytesize 2.6
libcaca 0.99.beta20
libcanberra 1:0.30+r2+gc0620e4
libcap 2.64
libcap-ng 0.8.3
libcddb 1.3.2
libcdio 2.1.0
libcdio-paranoia 10.2+2.0.1
libcdr 0.1.7
libcerf 1:2.1
libcloudproviders 0.3.1
libcolord 1.4.6
libconfig 1.7.3
libcroco 0.6.13
libcups 1:2.4.2
libdaemon 0.14
libdatrie 0.2.13
libdbusmenu-glib 16.04.0
libdbusmenu-gtk3 16.04.0
libdbusmenu-qt5 0.9.3+16.04.20160218
libdc1394 2.2.6
libdca 0.0.7
libde265 1.0.8
libdiscid 0.6.2
libdrm 2.4.111
libdv 1.0.0
libdvbpsi 1:1.3.3
libdvdcss 1.4.3
libdvdnav 6.1.1
libdvdread 6.1.3
libe-book 0.1.3
libebml 1.4.2
libedit 20210910_3.1
libelf 0.187
libepoxy 1.5.10
libepubgen 0.1.1
libetonyek 0.1.10
libev 4.33
libevdev 1.12.1
libevent 2.1.12
libexif 0.6.24
libexttextcat 3.4.6
libfdk-aac 2.0.2
libffi 3.4.2
libfilezilla 1:0.37.2
libfm 1.3.2
libfm-extra 1.3.2
libfm-gtk2 1.3.2
libfontenc 1.1.4
libfreeaptx 0.1.1
libfreehand 0.1.2
libgcrypt 1.10.1
libgexiv2 0.14.0
libgit2 1:1.4.3
libglvnd 1.4.0
libgme 0.6.3
libgpg-error 1.45
libgphoto2 2.5.29
libgtop 2.40.0+2+g31db82ef
libgudev 237
libgusb 0.3.10
libhandy 1.6.2
libheif 1.12.0
libibus 1.5.26
libical 3.0.14
libice 1.0.10
libid3tag 0.15.1b
libidn 1.38
libidn2 2.3.2
libiec61883 1.2.0
libieee1284 0.2.11
libimagequant 2.17.0
libimobiledevice 1.3.0
libindicator-gtk3 12.10.1
libinih 55
libinput 1.20.1
libinstpatch 1.1.6
libisl 0.24
libisofs 1.5.4
libixion 0.17.0
libjpeg-turbo 2.1.3
libkate 0.4.1
libkeybinder3 0.3.2
libksba 1.6.0
liblangtag 0.6.3
libldac 2.0.2.3
libldap 2.6.2
liblouis 3.22.0
liblqr 0.4.2
liblrdf 0.6.1
libltc 1.3.1
libmad 0.15.1b
libmanette 0.2.6
libmatroska 1.6.3
libmd 1.0.4
libmfx 22.4.2
libmicrodns 0.2.0
libmm-glib 1.18.8
libmms 0.6.4
libmng 2.0.3
libmnl 1.0.5
libmodplug 0.8.9.0
libmpc 1.2.1
libmpcdec 1:0.1+r475
libmpeg2 0.5.1
libmspub 0.1.4
libmwaw 0.3.21
libmypaint 1.6.1
libndp 1.8
libnet 1:1.1.6
libnetfilter_conntrack 1.0.9
libnewt 0.52.21
libnfnetlink 1.0.2
libnftnl 1.2.2
libnghttp2 1.47.0
libnice 0.1.19
libnl 3.6.0
libnm 1.38.0
libnma 1.8.38
libnma-common 1.8.38
libnotify 0.7.12
libnsl 2.0.0
libnumbertext 1.0.10
libodfgen 0.1.8
libogg 1.3.5
libomxil-bellagio 0.9.3
libopenmpt 0.6.3
liborcus 0.17.2
libp11-kit 0.24.1
libpagemaker 0.0.4
libpamac 11.3.1
libpaper 1.1.28
libpcap 1.10.1
libpciaccess 0.16
libpgm 5.3.128
libpipeline 1.5.6
libplacebo 4.192.1
libplist 2.2.0
libpng 1.6.37
libproxy 0.4.17
libpsl 0.21.1
libpulse 16.0
libqxp 0.0.2
libraqm 0.9.0
libraw 0.20.2
libraw1394 2.1.2
libreoffice-fresh 7.3.4
librevenge 0.0.4
librsvg 2:2.54.3
libsamplerate 0.2.2
libsasl 2.1.28
libseccomp 2.5.4
libsecret 0.20.5
libshout 1:2.4.6
libsidplay 1.36.59
libsigc++ 2.10.8
libsm 1.2.3
libsndfile 1.1.0
libsodium 1.0.18
libsoup 2.74.2
libsoup3 3.0.6
libsoxr 0.1.3
libspiro 1:20200505
libsrtp 1:2.4.2
libssh 0.9.6
libssh2 1.10.0
libstaroffice 0.0.7
libstemmer 2.2.0
libsysprof-capture 3.44.0
libtar 1.2.20
libtasn1 4.18.0
libteam 1.31
libthai 0.1.29
libtheora 1.1.1
libtiff 4.4.0
libtirpc 1.3.2
libtommath 1.2.0
libtool 2.4.7
libtorrent-rasterbar 1:2.0.6
libunistring 1.0
libunwind 1.6.2
libupnp 1.14.12
liburcu 0.13.1
liburing 2.1
libusb 1.0.26
libusbmuxd 2.0.2
libutempter 1.2.1
libuv 1.43.0
libva 2.14.0
libvdpau 1.5
libverto 0.3.2
libvisio 0.1.7
libvisual 0.4.0
libvncserver 0.9.13
libvorbis 1.3.7
libvpx 1.11.0
libwacom 2.2.0
libwebp 1.2.2
libwmf 0.2.12
libwnck3 40.1
libwpd 0.10.3
libwpe 1.12.0
libwpg 0.3.3
libwps 0.4.12
libx11 1.8.1
libx86emu 3.5
libxau 1.0.9
libxaw 1.0.14
libxcb 1.15
libxcomposite 0.4.5
libxcrypt 4.4.28
libxcursor 1.2.1
libxcvt 0.1.1
libxdamage 1.1.5
libxdmcp 1.1.3
libxext 1.3.4
libxfce4ui 4.16.1
libxfce4util 4.16.0
libxfixes 6.0.0
libxfont2 2.0.5
libxft 2.3.4
libxi 1.8
libxinerama 1.1.4
libxkbcommon 1.4.1
libxkbcommon-x11 1.4.1
libxkbfile 1.1.0
libxklavier 5.4
libxml2 2.9.14
libxmu 1.1.3
libxnvctrl 515.48.07
libxpm 3.5.13
libxpresent 1.0.0+2+gdd6771c
libxrandr 1.5.2
libxrender 0.9.10
libxres 1.2.1
libxshmfence 1.3
libxslt 1.1.35
libxss 1.2.3
libxt 1.2.1
libxtst 1.2.3
libxv 1.0.11
libxxf86vm 1.1.4
libyaml 0.2.5
libyuv r2322+3aebf69d
libzip 1.8.0
libzmf 0.0.2
licenses 20220125
lightdm 1:1.30.0
lightdm-settings 1.5.7
lightdm-slick-greeter 1.5.7
lilv 0.24.14
linux-api-headers 5.17.5
linux-firmware 20220509.b19cbdc
linux-firmware-whence 20220509.b19cbdc
linux510 5.10.121
linux510-headers 5.10.121
linux510-nvidia 515.48.07
linux510-virtualbox-host-modules 6.1.34
llvm-libs 13.0.1
lm_sensors 1:3.6.0.r41.g31d1f125
lmdb 0.9.29
logrotate 3.19.0
lpsolve 5.5.2.11
lsb-release 2.0.r48.3cf5103
lshw B.02.19.2
lua 5.4.4
lua52 5.2.4
lua53 5.3.6
luajit 2.1.0.beta3.r411.g68bb1140
luit 20220111
lv2 1.18.4
lvm2 2.03.16
lxappearance 0.6.3
lxinput 0.3.5
lxmenu-data 0.1.5
lz4 1:1.9.3
lzo 2.10
m4 1.4.19
maia-console 1.2
make 4.3
man-db 2.10.2
man-pages 5.13
manjaro-alsa 20210928
manjaro-application-utility 1.3.3
manjaro-browser-settings 20220522
manjaro-firmware 20160419
manjaro-hotfixes 2018.08
manjaro-icons 20191015
manjaro-keyring 20220514
manjaro-printer 20220314
manjaro-release 21.2.6
manjaro-settings-manager 0.5.7
manjaro-settings-manager-notifier 0.5.7
manjaro-system 20220202
manjaro-zsh-config 0.22
markdown_previewer 0.r6.64ad4d7
matcha-gtk-theme 20220607
md4c 0.4.8
mdadm 4.2
meld 3.20.4
memtest86+ 5.31b
menu-cache 1.1.0
mesa 22.1.1
mesa-demos 8.5.0
metis 5.1.0.p10
mhwd 0.6.5
mhwd-amdgpu 19.1.0
mhwd-ati 19.1.0
mhwd-db 0.6.5
mhwd-nvidia 515.48.07
mhwd-nvidia-390xx 390.151
mhwd-nvidia-470xx 470.129.06
minizip 1:1.2.12
mjpegtools 2.2.1
mkinitcpio 31
mkinitcpio-busybox 1.35.0
mkinitcpio-openswap 0.1.0
mobile-broadband-provider-info 20220511
moc 1:2.5.2
morc_menu 1.0
mousepad 0.5.9
mpfr 4.1.0.p13
mpg123 1.29.3
mtdev 1.1.6
mypaint-brushes1 1.3.1
nano 6.3
ncdu 2.1.2
ncftp 3.2.6
ncurses 6.3
ndctl 73
neon 0.32.2
nerd-fonts-noto-sans-mono 2.1.0
net-snmp 5.9.1
netctl 1.28
netpbm 10.73.37
nettle 3.8
network-manager-applet 1.28.0
networkmanager 1.38.0
networkmanager-openconnect 1.2.8
networkmanager-openvpn 1.8.18
networkmanager-pptp 1.2.10
networkmanager-vpnc 1.2.8
nfs-utils 2.6.1
nfsidmap 2.6.1
nload 0.7.4
nm-connection-editor 1.28.0
nmap 7.92
nodejs 18.3.0
noto-fonts 20220502
npth 1.6
nspr 4.34
nss 3.79
nss-mdns 0.15.1
ntfs-3g 2022.5.17
ntp 4.2.8.p15
numactl 2.0.14
nvidia-prime 1.0
nvidia-utils 515.48.07
oath-toolkit 2.6.7
ocaml 4.13.1
oniguruma 6.9.8
openal 1.22.0
openconnect 1:9.01
opencore-amr 0.1.5
openexr 3.1.5
openjpeg2 2.5.0
openresolv 3.12.0
openssh 9.0p1
openssl 1.1.1.o
openvpn 2.5.7
optipng 0.7.7
opus 1.3.1
orc 0.4.32
os-prober 1.79
p11-kit 0.24.1
p7zip 1:17.04
pacman 6.0.1
pacman-mirrors 4.23.2
pacui 1.14.r39.ga37e948
pahole 1.23
pam 1.5.2
pamac-cli 10.4.1
pamac-gtk 10.4.1
pambase 20211210
pango 1:1.50.7
pangomm 2.46.2
papirus-icon-theme 20220606
papirus-maia-icon-theme 20200702
parted 3.5
patch 2.7.6
patchutils 0.4.2
pavucontrol 1:5.0+r35+g964f298
pciutils 3.8.0
pcmanfm 1.3.2
pcre 8.45
pcre2 10.40
pcsclite 1.9.7
perl 5.36.0
perl-alien-build 2.48
perl-alien-libxml2 0.17
perl-capture-tiny 0.48
perl-clone 0.45
perl-dbi 1.643
perl-encode-locale 1.05
perl-error 0.17029
perl-ffi-checklib 0.28
perl-file-basedir 0.09
perl-file-chdir 0.1011
perl-file-desktopentry 0.22
perl-file-listing 6.15
perl-file-mimeinfo 0.32
perl-file-which 1.27
perl-html-parser 3.78
perl-html-tagset 3.20
perl-http-cookies 6.10
perl-http-daemon 6.14
perl-http-date 6.05
perl-http-message 6.36
perl-http-negotiate 6.01
perl-io-html 1.004
perl-ipc-system-simple 1.30
perl-libwww 6.60
perl-lwp-mediatypes 6.04
perl-mailtools 2.21
perl-net-http 6.22
perl-parse-yapp 1.21
perl-path-tiny 0.122
perl-timedate 2.33
perl-try-tiny 0.31
perl-uri 5.10
perl-www-robotrules 6.02
perl-xml-libxml 2.0207
perl-xml-namespacesupport 1.12
perl-xml-parser 2.46
perl-xml-sax 1.02
perl-xml-sax-base 1.09
perl-xml-writer 0.900
phonon-qt5 4.11.1
phonon-qt5-gstreamer 4.10.0
php 8.1.7
picom 9.1
pinentry 1.2.0
pipewire 1:0.3.52
pipewire-pulse 1:0.3.52
pixman 0.40.0
pkcs11-helper 1.29.0
pkgconf 1.8.0
pkgfile 21
polkit 0.120
polkit-gnome 0.105
polkit-qt5 0.114.0
poppler 22.06.0
poppler-data 0.4.11
poppler-glib 22.06.0
popt 1.18
portaudio 1:19.7.0
postgresql 14.3
postgresql-libs 14.3
postman-bin 9.19.0
potrace 1.16
powertop 2.14
ppp 2.4.9
pptpclient 1.10.0
procps-ng 3.3.17
psmisc 23.5
python 3.10.5
python-appdirs 1.4.4
python-cachecontrol 1:0.12.6
python-cairo 1.21.0
python-certifi 2021.10.8
python-cffi 1.15.0
python-chardet 4.0.0
python-colorama 0.4.4
python-configobj 5.0.6.r110.g3e2f4cc
python-contextlib2 0.6.0.post1
python-cryptography 37.0.2
python-dateutil 2.8.2
python-distlib 0.3.4
python-distro 1.7.0
python-dnspython 1:2.2.1
python-gobject 3.42.1
python-html5lib 1.1
python-idna 3.3
python-importlib-metadata 4.8.1
python-markdown 3.3.7
python-more-itertools 8.12.0
python-msgpack 1.0.3
python-npyscreen 4.10.5
python-ordered-set 4.0.2
python-packaging 21.3
python-pep517 0.12.0
python-pillow 9.1.1
python-pip 21.0
python-ply 3.11
python-progress 1.6
python-psutil 5.9.0
python-pyalsa 1.2.7
python-pyaml 21.10.1
python-pycparser 2.21
python-pyopenssl 22.0.0
python-pyparsing 3.0.9
python-pyqt5 5.15.6
python-pyqt5-sip 12.10.1
python-pysmbc 1.0.23
python-reportlab 3.6.9
python-requests 2.27.1
python-resolvelib 0.5.5
python-retrying 1.3.3
python-setproctitle 1.2.3
python-setuptools 1:60.6.0
python-six 1.16.0
python-toml 0.10.2
python-tomli 2.0.1
python-urllib3 1.26.9
python-webencodings 0.5.1
python-xapp 2.2.1
python-yaml 6.0
python-zipp 3.8.0
python2 2.7.18
qbittorrent 4.4.3.1
qpdf 10.6.3
qrencode 4.1.1
qt5-base 5.15.4+kde+r164
qt5-declarative 5.15.4+kde+r19
qt5-location 5.15.4+kde+r2
qt5-multimedia 5.15.4+kde+r1
qt5-sensors 5.15.4+kde+r0
qt5-speech 5.15.4+kde+r1
qt5-styleplugins 5.0.0.20170311
qt5-svg 5.15.4+kde+r10
qt5-tools 5.15.4+kde+r1
qt5-translations 5.15.4+kde+r2
qt5-wayland 5.15.4+kde+r38
qt5-webchannel 5.15.4+kde+r3
qt5-webkit 5.212.0alpha4
qt5-x11extras 5.15.4+kde+r0
qt5ct 1.5
qt6-base 6.3.0
qt6-svg 6.3.0
qt6-translations 6.3.0
ragel 6.10
ranger 1.9.3+428+g0b77fb8a
raptor 2.0.15
rasqal 1:0.9.33
rav1e 0.4.1
re2 1:20220601
readline 8.1.002
redland 1:1.0.17
reiserfsprogs 3.6.27
rest 0.8.1+r4+ge5ee6ef
rpcbind 1.2.6
rsync 3.2.4
rtl8821cu-dkms-git 5.4.11202e43634
rtmpdump 1:2.4.r99.f1b83c1
run-parts 5.5
runc 1.1.3
rust 1:1.61.0
rxvt-unicode 9.26
rxvt-unicode-terminfo 9.26
s-nail 14.9.24
samba 4.16.1
sane 1.1.1
sbc 1.5
sbxkb 0.7.6
schemacrawler 16.16.12
screenfetch 3.9.1
scrot 1.7
sdl12-compat 1.2.52
sdl2 2.0.22
sed 4.8
serd 0.30.12
shaderc 2022.1
shadow 4.11.1
shared-mime-info 2.0+144+g13695c7
shellcheck 0.8.0
simplescreenrecorder 0.4.4
slack-desktop 4.26.1
slang 2.3.2
smbclient 4.16.1
snapd 2.56
snappy 1.1.9
sndpeek-alsa 1.4
sord 0.16.10
sound-theme-freedesktop 0.8
soundtouch 2.3.1
spandsp 0.0.6
spectre-meltdown-checker 0.45
speedtest-cli 2.1.3
speex 1.2.0
speexdsp 1.2.0
spirv-tools 2022.1
splix 2.0.0
sqlcipher 4.5.1
sqlite 3.38.5
squashfs-tools 4.5.1
sratom 0.6.10
srt 1.4.4
sshfs 3.7.3
startup-notification 0.12
stoken 0.92
sudo 1.9.11.p1
suitesparse 5.12.0
svt-av1 0.9.0
svt-hevc 1.5.1
sweet-theme-git 3.0.r46.g39fba74
sysfsutils 2.1.1
syslog-ng 3.36.1
systemd 251.2
systemd-fsck-silent 239
systemd-libs 251.2
systemd-sysvcompat 251.2
taglib 1.12
talloc 2.3.4
tar 1.34
tdb 1.4.7
terminator 2.1.1
terminus-font 4.49.1
tevent 1:0.12.1
texinfo 6.8
thin-provisioning-tools 0.9.0
tigervnc 1.12.0
tlp 1.5.0
tpm2-tss 3.2.0
traceroute 2.1.0
tracker3 3.3.1
tree 2.0.2
tslib 1.22
ttf-bitstream-vera 1.10
ttf-dejavu 2.37+18+g9b5d1b2f
ttf-droid 20121017
ttf-fira-code 6.2
ttf-fira-mono 2:3.206
ttf-fira-sans 1:4.301
ttf-font-icons 1.1
ttf-inconsolata 1:3.000
ttf-indic-otf 0.2
ttf-liberation 2.1.5
ttf-vlgothic 20200720
twolame 0.4.0
tzdata 2022a
udisks2 2.9.4
ufw 0.36.1
unrar 1:6.1.7
unzip 6.0
upower 0.99.19
urxvt-perls 2.3
usbmuxd 1.1.1
usbutils 014
util-linux 2.38
util-linux-libs 2.38
v4l-utils 1.22.1
v86d 0.1.10
vertex-maia-themes 20180519
vi 1:070224
vibrancy-icons-teal 2.7
vid.stab 1.1
virtualbox 6.1.34
virtualbox-host-dkms 6.1.34
vlc 3.0.17.4
vmaf 2.3.1
volume_key 0.3.12
volumeicon 0.5.1
vpnc 1:0.5.3.r506.r204
vte-common 0.68.0
vte3 0.68.0
vulkan-headers 1:1.3.213
vulkan-icd-loader 1.3.213
w3m 0.5.3.git20220409_1
wallpapers-juhraya 1.1
wavpack 5.4.0
wayland 1.20.0
wayland-protocols 1.25
webkit2gtk 2.36.3
webrtc-audio-processing 0.3.1
wget 1.21.3
which 2.21
wildmidi 0.4.4
wireless-regdb 2022.06.06
wireless_tools 30.pre9
wireplumber 0.4.10
wmutils 1:1.4
woff2 1.0.2
wpa_supplicant 2:2.10
wpebackend-fdo 1.12.0
wxgtk-common 3.0.5.1
wxgtk3 3.0.5.1
x264 3:0.164.r3081.19856cc
x265 3.5
xarchiver 0.5.4.18
xautolock 2.2
xbitmaps 1.1.2
xcb-proto 1.15
xcb-util 0.4.0
xcb-util-cursor 0.1.3
xcb-util-image 0.4.0
xcb-util-keysyms 0.4.0
xcb-util-renderutil 0.3.9
xcb-util-wm 0.4.1
xcb-util-xrm 1.3
xclip 0.13
xcursor-breeze 5.22.5
xcursor-chameleon-pearl 0.5
xcursor-maia 20160417
xdg-dbus-proxy 0.1.4
xdg-user-dirs 0.17
xdg-utils 1.1.3+21+g1a58bc2
xdotool 3.20211022.1
xf86-input-elographics 1.4.2
xf86-input-evdev 2.10.6
xf86-input-libinput 1.2.1
xf86-input-void 1.4.1
xfburn 0.6.2
xfce-polkit 0.3
xfce4-notifyd 0.6.3
xfce4-power-manager 4.16.0
xfce4-settings 4.16.2
xfconf 4.16.0
xfsprogs 5.18.0
xfwm4 4.16.1
xfwm4-themes 4.10.0
xkeyboard-config 2.36
xmlsec 1.2.34
xorg-fonts-alias-misc 1.0.4
xorg-fonts-encodings 1.0.5
xorg-fonts-misc 1.0.3
xorg-mkfontscale 1.2.2
xorg-server 21.1.3
xorg-server-common 21.1.3
xorg-setxkbmap 1.3.3
xorg-twm 1.0.12
xorg-xauth 1.1.2
xorg-xbacklight 1.2.3
xorg-xdpyinfo 1.3.3
xorg-xev 1.2.4
xorg-xhost 1.0.8
xorg-xinit 1.4.1
xorg-xinput 1.6.3
xorg-xkbcomp 1.4.5
xorg-xkill 1.0.5
xorg-xmodmap 1.0.10
xorg-xprop 1.2.5
xorg-xrandr 1.5.1
xorg-xrdb 1.2.1
xorg-xset 1.2.4
xorg-xsetroot 1.1.2
xorg-xwininfo 1.1.5
xorgproto 2022.1
xsel 1.2.0.20200527
xterm 372
xvidcore 1.3.7
xxhash 0.8.1
xz 5.2.5
yajl 2.1.0
yarn 1.22.19
yay 11.1.2
yubico-c 1.13
yubico-c-client 2.15
yubikey-personalization 1.20.0
zbar 0.23.1
zenity 3.42.1
zensu 0.3
zeromq 4.3.4
zimg 3.0.3
zita-alsa-pcmi 0.4.0
zita-resampler 1.8.0
zlib 1:1.2.12
zoom 5.10.7
zsh 5.9
zsh-autosuggestions 0.7.0
zsh-completions 0.33.0
zsh-history-substring-search 1.0.2
zsh-syntax-highlighting 0.7.1
zsh-theme-powerlevel10k 1.16.1
zstd 1.5.2
zvbi 0.2.35
zxing-cpp 1.3.0
